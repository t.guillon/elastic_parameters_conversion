# elastic_parameters_conversion

## About

Just basic functions for converting between mechanical elastic parameters.

See https://en.wikipedia.org/wiki/Lam%C3%A9_parameters for the formulae.

## License
No specific one, feel free to use.
