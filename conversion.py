""" Conversion between elastic parameters

see https://en.wikipedia.org/wiki/Lam%C3%A9_parameters

"""
import numpy as np

def get_elastic_r(young_modulus, lambda_lame):
    """ The R constant derived from Young modulus and Lamé first parameter """
    return np.sqrt(
        np.power(young_modulus, 2)
        + 9*np.power(lambda_lame, 2)
        + 2*young_modulus*lambda_lame
        )

def bulk_modulus(
    bulk_modulus=None,
    young_modulus=None,
    lambda_lame=None,
    shear_modulus=None,
    poisson_ratio=None,
    p_wave_modulus=None
    ):
    if bulk_modulus:
        return bulk_modulus
    if young_modulus and lambda_lame:
        r = get_elastic_r(young_modulus, lambda_lame)
        return (young_modulus+3*lambda_lame+r)/6
    if young_modulus and shear_modulus:
        return (young_modulus*shear_modulus)/3/(3*shear_modulus-young_modulus)
    if young_modulus and poisson_ratio:
        return young_modulus/3/(1-2*poisson_ratio)

def shear_modulus(
    bulk_modulus=None,
    young_modulus=None,
    lambda_lame=None,
    shear_modulus=None,
    poisson_ratio=None,
    p_wave_modulus=None
    ):
    if shear_modulus:
        return shear_modulus
    if bulk_modulus and young_modulus:
        return (3*bulk_modulus*young_modulus)/(9*bulk_modulus-young_modulus)
    if young_modulus and poisson_ratio:
        return young_modulus/2/(1+poisson_ratio)

def young_modulus(
    bulk_modulus=None,
    young_modulus=None,
    lambda_lame=None,
    shear_modulus=None,
    poisson_ratio=None,
    p_wave_modulus=None
    ):
    if young_modulus:
        return young_modulus
    if bulk_modulus and shear_modulus:
        return 9*bulk_modulus*shear_modulus/(3*bulk_modulus+shear_modulus)
    if bulk_modulus and poisson_ratio:
        return 3*bulk_modulus*(1-2*poisson_ratio)

def poisson_ratio(
    bulk_modulus=None,
    young_modulus=None,
    lambda_lame=None,
    shear_modulus=None,
    poisson_ratio=None,
    p_wave_modulus=None
    ):
    if poisson_ratio:
        return poisson_ratio
    if bulk_modulus and young_modulus:
        return (3*bulk_modulus-young_modulus)/6/bulk_modulus
    if bulk_modulus and shear_modulus:
        return (3*bulk_modulus-2*shear_modulus)/2/(3*bulk_modulus+shear_modulus)
    
if __name__ == '__main__':

    bulk = 2.e9
    shear = 1.e6 #[sic]
    young = young_modulus(bulk_modulus=bulk, shear_modulus=shear)
    nu = poisson_ratio(bulk_modulus=bulk, shear_modulus=shear)
    
    young = 13.e9
    nu = 0.25
    bulk = bulk_modulus(young_modulus=young, poisson_ratio=nu)
    shear = shear_modulus(young_modulus=young, poisson_ratio=nu)
    print(bulk/shear)
    
    
    # global density = 2500
    # nu = (3*bulk-2*shear)/(2*(3*bulk+shear))
    # global UCS = 60.e6 ; from GIG
    # global tstrength = 6.e6 ; from GIG
    
    # ; FZs: assuming medium and homogeneously fractured matrix
    # global bulk_fz = 0.8*bulk
    # global shear_fz = 0.9*shear
    # global density_fz = 0.9*density
    # global fric_fz = math.atan(0.2) / math.degrad ;math.atan(0.6) / math.degrad ; ~31Ãƒâ€šÃ‚Â°
    # global cohe_fz = 0;10.e6
    # global tension_fz = 0.8*tstrength